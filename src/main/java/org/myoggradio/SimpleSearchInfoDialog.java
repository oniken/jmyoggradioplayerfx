package org.myoggradio;

import org.myoggradio.interfaces.SearchInfoDialog;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class SimpleSearchInfoDialog extends JDialog implements ActionListener, SearchInfoDialog
{
	public static final long serialVersionUID = 0;
	private File searchDirectory = null;
	private boolean shouldSearch = false;
	private JLabel extendSearchLabel = new JLabel("Should extend Search in:");
	private JButton yesOptionButton = new JButton("yes");
	private JButton noOptionButton = new JButton("no");
	private JPanel bpan = new JPanel();
	private JPanel fpan = new JPanel();
	private JPanel cpan = new JPanel();
	private ButtonGroup bg = new ButtonGroup();
	private JRadioButton[] buttons = null;
	private File[] roots = null;
	public boolean shouldSearch()
	{
		return shouldSearch;
	}
	public File searchDirectory()
	{
		return searchDirectory;
	}

	public void showDialog()
	{
		this.setModal(true);
		this.setTitle("myoggradio.org");
		buildfpan();
		bpan.setLayout(new GridLayout(1,2));
		bpan.add(yesOptionButton);
		bpan.add(noOptionButton);
		cpan.setLayout(new BorderLayout());
		cpan.add(extendSearchLabel,BorderLayout.NORTH);
		cpan.add(fpan,BorderLayout.CENTER);
		cpan.add(bpan,BorderLayout.SOUTH);
		yesOptionButton.addActionListener(this);
		noOptionButton.addActionListener(this);
		setContentPane(cpan);
		pack();
		setVisible(true);
	}
	private void buildfpan()
	{
		roots = File.listRoots();
		buttons = new JRadioButton[roots.length];
		fpan.setLayout(new GridLayout(roots.length,1));
		for (int i=0;i<roots.length;i++)
		{
			File root = roots[i];
			String pfad = root.getAbsolutePath();
			buttons[i] = new JRadioButton(pfad);
			bg.add(buttons[i]);
			fpan.add(buttons[i]);
			buttons[i].setSelected(true);
		}
	}
	public void actionPerformed(ActionEvent event)
	{
		Object source = event.getSource();
		if (source == yesOptionButton)
		{
			shouldSearch = true;
			for (int i=0;i<buttons.length;i++)
			{
				JRadioButton button = buttons[i];
				if (button.isSelected())
				{
					searchDirectory = roots[i];
				}
			}
			setVisible(false);
		}
		if (source == noOptionButton)
		{
			shouldSearch = false;
			setVisible(false);
		}
	}
	public void kill()
	{
		dispose();
	}
}
