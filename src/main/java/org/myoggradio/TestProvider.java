package org.myoggradio;

import org.myoggradio.interfaces.Provider;
import org.myoggradio.stringpair.StringPair;

import java.util.ArrayList;

public class TestProvider implements Provider
{
	private ArrayList<StringPair> alC = new ArrayList<StringPair>();
	private ArrayList<StringPair> alF = new ArrayList<StringPair>();

	public TestProvider()
	{
		StringPair sp1 = new StringPair("Radio Swiss Pop","http://zlz-stream11.streaming.init7.net:80/1/rsp/mp3_128");
		StringPair sp2 = new StringPair("Einslive","http://gffstream.ic.llnwd.net/stream/gffstream_stream_wdr_einslive_a ");
		alC.add(sp1);
		alC.add(sp2);
		alF.add(sp1);
	}
	public void addFavorite(StringPair sp, String user, String password)
	{
		alF.add(sp);
	}
	public void dropFavorite(StringPair sp, String user, String password)
	{
		alF.remove(sp);
	}
	public ArrayList<StringPair> getCommon()
	{
		return alC;
	}
	public ArrayList<StringPair> getFavoriten(String user, String password)
	{
		return alF;
	}

	public String login(String user, String password)
	{
		String erg = "ok";
		return erg;
	}
	@Override
	public void changeFavorite(StringPair sp, String user, String password)
	{
		// Nicht implementiert
	}
	@Override
	public void addNewURL(String url, String notice, String user,
			String password) {
		// TODO Auto-generated method stub
		
	}
}
