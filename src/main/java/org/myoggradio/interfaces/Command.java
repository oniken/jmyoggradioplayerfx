package org.myoggradio.interfaces;

public interface Command 
{
	int start(String command);
}
