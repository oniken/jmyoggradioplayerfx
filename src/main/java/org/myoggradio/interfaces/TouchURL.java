package org.myoggradio.interfaces;

public interface TouchURL 
{
	void touch(String url);
}
