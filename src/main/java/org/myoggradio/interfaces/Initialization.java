package org.myoggradio.interfaces;

public interface Initialization
{
	int start();
}
