package org.myoggradio.interfaces;

import java.io.File;

public interface SearchInfoDialog
{
	void show();
	void kill();
	boolean shouldSearch();
	File searchDirectory();
}
