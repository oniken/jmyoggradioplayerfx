package org.myoggradio.interfaces;

public interface Player 
{
	void play(String url);
}
