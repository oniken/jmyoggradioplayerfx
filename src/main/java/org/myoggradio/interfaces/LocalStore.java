package org.myoggradio.interfaces;

import org.myoggradio.stringpair.StringPair;
import java.util.ArrayList;

public interface LocalStore
{
	ArrayList<StringPair> getCommon();
	void putCommon(ArrayList<StringPair> al);
}
