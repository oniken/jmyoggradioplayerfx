package org.myoggradio.interfaces;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Protocol
{
	private static ArrayList<String> al = new ArrayList<String>();
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static void write(String s)
	{
		Date now = new Date();
		String prefix = sdf.format(now);
		System.out.println(prefix + " " + s);
		al.add(prefix + " " + s);
	}
	public static ArrayList<String> getMessages()
	{
		return al;
	}
}
