package org.myoggradio.interfaces;
import org.myoggradio.stringpair.StringPair;

import java.util.ArrayList;

public interface Provider 
{
	ArrayList<StringPair> getFavoriten(String user, String password);
	ArrayList<StringPair> getCommon();
	void addFavorite(StringPair pair, String user, String password);
	void dropFavorite(StringPair pair, String user, String password);
	void changeFavorite(StringPair pair, String user, String password);
	void addNewURL(String url, String notice, String user, String password);
	String login(String user, String password);
}
