package org.myoggradio.interfaces;

public interface NonBlockingSwingMessage 
{
	void show(String msg);
}
