package org.myoggradio.interfaces;

import org.myoggradio.stringpair.StringPair;

import java.util.ArrayList;

public interface Filter
{
	void reset();
	String get();
	void set(String s);
	ArrayList<StringPair> match(ArrayList<StringPair> al);
}