package org.myoggradio.interfaces;
import org.myoggradio.AUDACIOUS;
import org.myoggradio.AdvancedCommand;
import org.myoggradio.MyOggRadioProvider;
import org.myoggradio.PreferencesInitialization;
import org.myoggradio.SimpleBackgroundSearcher;
import org.myoggradio.SimpleFilter;
import org.myoggradio.SimpleLocalStore;
import org.myoggradio.SimpleNonBlockingSwingMessage;
import org.myoggradio.SimpleSearchInfoDialog;
import org.myoggradio.searcher.Searcher;
import org.myoggradio.searcher.SimpleSearcher;
import org.myoggradio.SimpleTouchURL;
import org.myoggradio.StreamRipper32;
import org.myoggradio.Streamripper;
import org.myoggradio.VLC;
import org.myoggradio.stringpair.StringPairPanel;
import org.myoggradio.views.swing.LinuxPreferencesMenu;
import org.myoggradio.views.swing.Menu;
import org.myoggradio.views.swing.MenuCommon;
import org.myoggradio.views.swing.MenuLogon;
import org.myoggradio.views.swing.MenuMain;
import org.myoggradio.views.swing.MenuNewURL;
import org.myoggradio.views.swing.SimpleFilterMenu;
import org.myoggradio.views.swing.SimpleProtocolMenu;
import org.myoggradio.views.swing.SimpleStringPairPanel;
import org.myoggradio.views.swing.WindowsPreferencesMenu;

import javax.swing.JDialog;

public class Factory
{
	public static Filter getFilter()
	{
		return new SimpleFilter();
	}
	public static LocalStore getLocalStore()
	{
		return new SimpleLocalStore();
	}
	public static TouchURL getTouchURL()
	{
		return new SimpleTouchURL();
	}
	public static SearchInfoDialog getSearchInfoDialog()
	{
		return new SimpleSearchInfoDialog();
	}
	public static BackgroundSearcher getBackgroundSearcher()
	{
		return new SimpleBackgroundSearcher();
	}
	public static NonBlockingSwingMessage getNonBlockingSwingMessage()
	{
		return new SimpleNonBlockingSwingMessage();
	}
	public static Command getCommand()
	{
		return new AdvancedCommand();
	}
	public static Menu getLinuxPreferencesMenu()
	{
		return new LinuxPreferencesMenu();
	}
	public static Menu getWindowsPreferencesMenu()
	{
		return new WindowsPreferencesMenu();
	}
	public static Initialization getInitialization()
	{
		return new PreferencesInitialization();
	}
	public static Menu getMenuCommon()
	{
		return new MenuCommon();
	}
	public static Menu getMenuNewURL()
	{
		return new MenuNewURL();
	}
	public static Menu getProtocolMenu()
	{
		return new SimpleProtocolMenu("Messages");
	}
	public static JDialog getFilterMenu()
	{
		return new SimpleFilterMenu();
	}
	public static Provider getProvider()
	{
		return new MyOggRadioProvider();
	}
	public static Menu getMenuLogon()
	{
		return new MenuLogon();
	}
	public static Menu getMenuMain()
	{
		return new MenuMain();
	}
	public static StringPairPanel getStringPaarPanel()
	{
		return new SimpleStringPairPanel();
	}
	public static Searcher getSearcher()
	{
		return new SimpleSearcher();
	}
	public static Player getPlayer()
	{
		String os = System.getProperty("os.name");
		Protocol.write("Factory:getPlayer:os:" + os);
		if (os == null) os = "Unkown";
		String id = os.substring(0,1).toUpperCase();
		if (id.equals("W"))
		{
			return new VLC();
		}
		else if (id.equals("L"))
		{
			return new AUDACIOUS();
		}
		else
		{
			Protocol.write("Factory:getPlayer:Operatingsystem unkown");
		}
		return null;
	}
	public static Recorder getRecorder()
	{
		String os = System.getProperty("os.name");
		Protocol.write("Factory:getRecorder:os:" + os);
		if (os == null) os = "Unkown";
		String id = os.substring(0,1).toUpperCase();
		if (id.equals("W"))
		{
			return new StreamRipper32();
		}
		else if (id.equals("L"))
		{
			return new Streamripper();
		}
		else
		{
			Protocol.write("Factory:getRecorder:Operatingsystem unkown");
		}
		return null;
	}
}
