package org.myoggradio.interfaces;

public interface Recorder 
{
	void record(String url);
}
