package org.myoggradio.interfaces;

import java.io.File;

public interface BackgroundSearcher
{
	void search(File dir);
}
