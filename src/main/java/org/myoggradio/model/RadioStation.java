package org.myoggradio.model;

public class RadioStation {

    private final String name;
    private final String streamURL;

    public RadioStation(String name, String streamURL) {
        this.name = name;
        this.streamURL = streamURL;
    }
}
