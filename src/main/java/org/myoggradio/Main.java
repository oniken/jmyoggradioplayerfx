package org.myoggradio;
import org.myoggradio.interfaces.*;
import org.myoggradio.views.swing.Menu;

public class Main
{
	public static void main(String[] args) 
	{
		Initialization ini = Factory.getInitialization();
		int rc = ini.start();
		Protocol.write("Initialization ReturnCode:" + rc);
		Menu m = Factory.getMenuCommon();
		m.show();
	}
}
