package org.myoggradio.stringpair;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class StringPairModel extends AbstractTableModel
{
	static final long serialVersionUID = 1;
	String[] columnNames = new String[2];
	ArrayList<StringPair> paare = null;
	ArrayList<StringPairPanelListener> listener = null;
	public StringPairModel(ArrayList<StringPair> al, ArrayList<StringPairPanelListener> listener)
	{
		this.listener = listener;
		paare = al;
		columnNames[0] = "Radio Transmitter";
		columnNames[1] = "URL";
	}
	public Object getValueAt(int row,int col)
	{
		StringPair sp = paare.get(row);
		Object o = new Object();
		if (col == 0)
		{
			o = sp.getName();
		}
		if (col == 1)
		{
			o = sp.getWert();
		}
		return o;
	}
	public int getRowCount()
	{
		int i = paare.size();
		return i;
	}
	public int getColumnCount()
	{
		int i = 2;
		return i;
	}
	public String getColumnName(int col)
	{
		return columnNames[col];
	}
	public void setValueAt(Object value,int row,int col)
	{
		StringPair alt = paare.get(row);
		StringPair neu = new StringPair();
		neu.setName(alt.getName());
		neu.setWert(alt.getWert());
		if (value instanceof String)
		{
			String s = (String) value;
			if (col == 0) neu.setName(s);
			if (col == 1) neu.setWert(s);
			for (int i=0;i<listener.size();i++)
			{
				StringPairPanelListener sppl = listener.get(i);
				sppl.changed(alt,neu);
			}
		}
	}
	public boolean isCellEditable(int row,int col)
	{
		return true;
	}
}
