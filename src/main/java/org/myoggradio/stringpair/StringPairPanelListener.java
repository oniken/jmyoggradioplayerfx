package org.myoggradio.stringpair;

public interface StringPairPanelListener
{
	void changed(StringPair oldPair, StringPair newPair);
}
