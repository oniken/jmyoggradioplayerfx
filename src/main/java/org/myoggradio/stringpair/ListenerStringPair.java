package org.myoggradio.stringpair;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ListenerStringPair implements ListSelectionListener
{
	int last = 0;
	private JTable tab;
	public StringPair selectedPaar = null;
	static final long serialVersionUID = 1;
	public ListenerStringPair(JTable tab)
	{
		this.tab = tab;
	}
	public void valueChanged(ListSelectionEvent e)
	{
		if (e.getValueIsAdjusting())
		{
			int FI = e.getFirstIndex();
			int LI = e.getLastIndex();
			if (last == FI)
			{
				last = LI;
			}
			else
			{
				last = FI;
			}
			StringPairModel GM = (StringPairModel) tab.getModel();
			selectedPaar = GM.paare.get(last);
		}
	}
}
