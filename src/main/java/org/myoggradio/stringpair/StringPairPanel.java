package org.myoggradio.stringpair;

import javax.swing.JPanel;
import java.util.ArrayList;

public abstract class StringPairPanel extends JPanel
{
	public abstract void setName(String name);
	public abstract void setPair(ArrayList<StringPair> al);
	public abstract StringPair getSelected();
	public abstract void addListener(StringPairPanelListener listener);
}
