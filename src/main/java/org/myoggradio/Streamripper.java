package org.myoggradio;

import org.myoggradio.interfaces.Command;
import org.myoggradio.interfaces.Factory;
import org.myoggradio.interfaces.Recorder;

public class Streamripper extends Thread implements Recorder
{
	private String streamripper = Global.linux_recordercommand;
	private String url = "";
	public void record(String url)
	{
		this.url = url;
		this.start();
	}
	public void run()
	{
		String suffix = "";
		if (!Global.linux_recorderdirectory.equals(""))
		{
			suffix = " -d " + Global.linux_recorderdirectory;
		}
		String cmd = streamripper + " " + url + suffix;
		Command command = Factory.getCommand();
		command.start(cmd);
	}
}
