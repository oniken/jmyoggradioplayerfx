package org.myoggradio;

import org.myoggradio.interfaces.BackgroundSearcher;
import org.myoggradio.interfaces.Command;
import org.myoggradio.interfaces.Factory;
import org.myoggradio.interfaces.Player;
import org.myoggradio.interfaces.Protocol;
import org.myoggradio.interfaces.SearchInfoDialog;
import org.myoggradio.searcher.Searcher;

public class VLC extends Thread implements Player
{
	private String url = "";
	private String vlc = null;
	private String exe = Global.windows_playercommand;

	public void play(String url)
	{
		this.url = url;
		this.start();
	}

	public void run()
	{
		if (vlc == null)
		{
			Searcher searcher = Factory.getSearcher();
			vlc = searcher.search(exe,"jmyoggradioplayer_player");
		}
		if (vlc == null)
		{
			Protocol.write("VLC:run: " + exe + " not found");
			SearchInfoDialog sid = Factory.getSearchInfoDialog();
			sid.show();
			if (sid.shouldSearch())
			{
				BackgroundSearcher bs = Factory.getBackgroundSearcher();
				bs.search(sid.searchDirectory());
				sid.kill();
			}
		}
		else
		{
			String cmd = vlc + " " + url;
			Command command = Factory.getCommand();
			command.start(cmd);
		}
    }
}
