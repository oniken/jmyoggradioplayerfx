package org.myoggradio;

import org.myoggradio.interfaces.BackgroundSearcher;
import org.myoggradio.interfaces.Command;
import org.myoggradio.interfaces.Factory;
import org.myoggradio.interfaces.Protocol;
import org.myoggradio.interfaces.Recorder;
import org.myoggradio.interfaces.SearchInfoDialog;
import org.myoggradio.searcher.Searcher;

public class StreamRipper32 extends Thread implements Recorder
{
	private String url = "";
	private String ripper = null;
	private String exe = Global.windows_recordercommand;
	public void record(String url)
	{
		this.url = url;
		this.start();
	}
	public void run()
	{
		if (ripper == null)
		{
			Searcher searcher = Factory.getSearcher();
			ripper = searcher.search(exe,"jmyoggradioplayer_recorder");
		}
		if (ripper == null)
		{
			Protocol.write("StreamRipper32:run: " + exe + " not found");
			SearchInfoDialog sid = Factory.getSearchInfoDialog();
			sid.show();
			if (sid.shouldSearch())
			{
				BackgroundSearcher bs = Factory.getBackgroundSearcher();
				bs.search(sid.searchDirectory());
			}
		}
		else
		{
			String suffix = "";
			if (!Global.windows_recorderdirectory.equals(""))
			{
				suffix = " -d " + Global.windows_recorderdirectory;
			}
			String cmd = ripper + " " + url + suffix;
			Command command = Factory.getCommand();
			command.start(cmd);
		}
		return;
	}
}
