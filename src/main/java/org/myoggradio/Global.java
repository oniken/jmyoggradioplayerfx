package org.myoggradio;

public class Global 
{
	public static String version = "0.86 - 10.01.2013";
	public static int minCommon = 25;
	public static long stopDelta = 1500;
	public static Process process = null;
	public static String linux_playercommand = "vlc";
	public static String linux_recordercommand = "streamripper";
	public static String linux_recorderdirectory = "";
	public static String windows_playercommand = "vlc.exe";
	public static String windows_recordercommand = "streamripper.exe";
	public static String windows_recorderdirectory = "C:\\";
	public static String linux_playercommand_default = "vlc";
	public static String linux_recordercommand_default = "streamripper";
	public static String linux_recorderdirectory_default = "";
	public static String windows_playercommand_default = "vlc.exe";
	public static String windows_recordercommand_default = "streamripper.exe";
	public static String windows_recorderdirectory_default = "C:\\";
	public static String myoggradio = "http://ehm.homelinux.org/MyOggRadio";
	// public static String myoggradio = "http://localhost:8180/MyOggRadio";
	public static String user = "";
	public static String password = "";
	public static String filter = "";
}
