package org.myoggradio;

import org.myoggradio.interfaces.Command;
import org.myoggradio.interfaces.Factory;
import org.myoggradio.interfaces.Player;

public class AUDACIOUS extends Thread implements Player
{
	private String audacious = Global.linux_playercommand;
	private String url = "";
	public void play(String url)
	{
		this.url = url;
		this.start();
	}
	public void run()
	{
		String cmd = audacious + " " + url;
		Command command = Factory.getCommand();
		command.start(cmd);
	}
}
