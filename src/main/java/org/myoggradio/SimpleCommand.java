package org.myoggradio;
import org.myoggradio.interfaces.*;
import java.io.*;
public class SimpleCommand implements Command
{
	public int start(String command)
	{
		int rc = 0;
		if (Global.process != null)
		{
			Protocol.write("SimpleCommand:Command already running");
			rc = 4;
			return rc;
		}
		Runtime rt = Runtime.getRuntime();
		try
		{
			Protocol.write("SimpleCommand:start:" + command);
			Process p = rt.exec(command);
			Global.process = p;
			InputStream is = p.getInputStream();
			Reader r = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(r);
			String satz = br.readLine();
			int n = 0;
			while (satz != null)
			{
				n++;
				if (n > 15)
				{
					Protocol.write("SimpleCommand:write no more messages");
					break;
				}
				Protocol.write(satz);
				satz = br.readLine();
			}
			rc = p.waitFor();
			Protocol.write("SimpleCommand:end with Return Code:" + rc);
		}
		catch (Exception e)
		{
			Protocol.write("SimpleCommand:start:Exception:");
			Protocol.write(e.toString());
		}
		if (Global.process != null)
		{
			Global.process.destroy();
		}
		Global.process = null;
		return rc;
	}
}
