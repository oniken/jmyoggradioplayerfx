package org.myoggradio.searcher;

public interface Searcher 
{
	String search(String command, String preference);
}
