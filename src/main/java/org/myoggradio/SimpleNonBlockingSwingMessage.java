package org.myoggradio;

import org.myoggradio.interfaces.NonBlockingSwingMessage;

import javax.swing.JOptionPane;

public class SimpleNonBlockingSwingMessage extends Thread implements NonBlockingSwingMessage
{
	private String msg = null;
	public void show(String msg)
	{
		this.msg = msg;
		start();
	}
	public void run()
	{
		JOptionPane.showMessageDialog(null,msg);
	}
}
