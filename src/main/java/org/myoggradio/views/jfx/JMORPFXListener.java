package org.myoggradio.views.jfx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableView;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * @author Cronut
 */
public class JMORPFXListener implements Initializable {

    private static Logger log = Logger.getLogger(JMORPFXListener.class.getSimpleName());

    @FXML private Label listNameLabel;
    @FXML private ScrollPane tableScrollPane;
    @FXML private TableView radioStationTable;
    @FXML private Button startStreamButton;
    @FXML private Button stopStreamButton;
    @FXML private Button recordStreamButton;
    @FXML private Button loginButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        radioStationTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        bindTableViewSizeToScrollPane();
    }

    private void bindTableViewSizeToScrollPane() {
        radioStationTable.prefHeightProperty().bind(tableScrollPane.heightProperty());
        radioStationTable.prefWidthProperty().bind(tableScrollPane.widthProperty());
    }

    public void recordStream(ActionEvent actionEvent) throws NotImplementedException {
        log.info("Task not implemented.");
    }

    public void login(ActionEvent actionEvent) {
        log.info("Task not implemented.");
    }

    public void stopStream(ActionEvent actionEvent) {
        log.info("Task not implemented.");
    }

    public void startStream(ActionEvent actionEvent) {
        log.info("Task not implemented.");
    }
}
