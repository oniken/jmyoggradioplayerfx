package org.myoggradio.views.jfx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Logger;

public class JMORPFX extends Application {

    private static Logger log = Logger.getLogger(JMORPFX.class.getSimpleName());

    public static void main(String[] args) throws IOException {
        log.setUseParentHandlers(false);
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/jmorp_main.fxml"));
            Scene scene = new Scene(root, 640, 480);

            primaryStage.setTitle("JMORPFX");
            primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images.logo/jmorp_logo_16px.png")));
            primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images.logo/jmorp_logo_32px.png")));
            primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images.logo/jmorp_logo_64px.png")));
            primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images.logo/jmorp_logo_128px.png")));
            primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images.logo/jmorp_logo_256px.png")));
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
    }
}
