package org.myoggradio.views.swing;
import org.myoggradio.interfaces.Protocol;

import javax.swing.*;
import java.awt.*;
import java.util.*;
@SuppressWarnings("serial")
public class SimpleProtocolMenu extends Menu
{
	private JPanel cpan = new JPanel();
	private JTextArea ta = new JTextArea(20,80);
	private JScrollPane sp = new JScrollPane(ta);
	public SimpleProtocolMenu(String name)
	{
		super(name);
		ArrayList<String> protokol = Protocol.getMessages();
		int n = protokol.size();
		for (int i=0;i<n;i++)
		{
			String satz = protokol.get(n - i -1);
			ta.append(satz + "\n");
		}
		cpan.setLayout(new BorderLayout());
		cpan.add(sp);
		frame.setContentPane(cpan);
	}
}
