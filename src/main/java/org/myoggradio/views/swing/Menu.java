package org.myoggradio.views.swing;
import javax.swing.*;
import java.awt.event.*;

/**
 * @author Christian Ehm <homer65x>
 * @modified_by Cronut
 */
public class Menu implements ActionListener
{
	static final long serialVersionUID = 1;

    public static final int STAYALIVE = 0;
    public static final int DISPOSE = 1;
    public static final int EXIT = 2;

	JFrame frame;

	public Menu(String name) {
		frame = new JFrame();
		frame.setTitle(name);
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}

	public void show() {
		WindowListener listener = new WindowAdapter() {
			public void windowClosing(WindowEvent event) {
				int ok = onClose();
				if (ok == Menu.DISPOSE) {
					event.getWindow().dispose();
				}
				else if (ok == Menu.DISPOSE) {
					System.exit(0);
				}
			}
		};
		frame.addWindowListener(listener);
        frame.pack();
        frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent event)
	{

	}

	public int onClose() {
		return Menu.DISPOSE;
	}
}
