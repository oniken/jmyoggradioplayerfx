package org.myoggradio.views.swing;
import java.util.*;
import javax.swing.*;
import java.awt.*;

import org.myoggradio.stringpair.ListenerStringPair;
import org.myoggradio.stringpair.StringPair;
import org.myoggradio.stringpair.StringPairModel;
import org.myoggradio.stringpair.StringPairPanel;
import org.myoggradio.stringpair.StringPairPanelListener;

@SuppressWarnings("serial")
public class SimpleStringPairPanel extends StringPairPanel
{
	private ArrayList<StringPairPanelListener> sppl = new ArrayList<StringPairPanelListener>();
	private String name = "SimpleStringPairPanel";
	private ArrayList<StringPair> paare = new ArrayList<StringPair>();
	private JTable table = null;
	private JScrollPane sp = null;
	private StringPairModel spM = null;
	private ListenerStringPair listenerSP = null;
	@Override
	public StringPair getSelected()
	{
		return listenerSP.selectedPaar;
	}
	@Override
	public void setName(String s) 
	{
		name = s;
	}
	@Override
	public void setPair(ArrayList<StringPair> al)
	{
		paare = al;		
		spM = new StringPairModel(paare,sppl);
		table = new JTable(spM);
		listenerSP = new ListenerStringPair(table);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.getSelectionModel().addListSelectionListener(listenerSP);
		sp = new JScrollPane(table);
		build();
	}
	public void build()
	{
		this.removeAll();
		setLayout(new BorderLayout());
		add(new JLabel(name),BorderLayout.NORTH);
		add(sp,BorderLayout.CENTER);
		this.setPreferredSize(new Dimension(480,480));
		this.validate();
	}
	@Override
	public void addListener(StringPairPanelListener listener)
	{
		sppl.add(listener);		
	}
}
