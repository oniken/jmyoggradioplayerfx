package org.myoggradio.views.swing;
import java.net.URL;
import java.util.*;
import java.util.prefs.Preferences;
import javax.swing.*;

import org.myoggradio.Global;
import org.myoggradio.stringpair.StringPair;
import org.myoggradio.interfaces.*;
import org.myoggradio.stringpair.StringPairPanel;

import java.awt.*;
import java.awt.event.*;

/**
 * @author homer65x
 * @modified_by Cronut
 */
public class MenuCommon extends Menu
{
	public static final long serialVersionUID = 0;
	private LocalStore localStore = Factory.getLocalStore();
	private ArrayList<StringPair> common = null;
	private StringPairPanel sppC = Factory.getStringPaarPanel();;
	private Provider provider = Factory.getProvider();
	private JPanel cpan = new JPanel();
	private JButton playStreamButton = new JButton("play");
	private JButton recordStreamButton = new JButton("record");
	private JButton stopStreamButton = new JButton("stop");
	private JButton loginButton = new JButton("login");
	private JButton showLogViewButton = new JButton("Messages");
	private JMenuBar menuBar = new JMenuBar();
	private JMenu infoMenu = new JMenu("Info");
	private JMenuItem versionMenuItem = new JMenuItem("Version");
	private JMenuItem wikiMenuItem = new JMenuItem("Wiki");
	private JMenu linuxSettingsMenu = new JMenu("Linux");
	private JMenu windowsSettingsMenu = new JMenu("Windows");
	private JMenuItem linuxPreferencesMenuItem = new JMenuItem("Preferences");
	private JMenuItem windowsPreferencesMenuItem = new JMenuItem("Preferences");
	private Preferences preferences = Preferences.userRoot();

	public MenuCommon() {
		super("http://www.myoggradio.org");

		int posX = 0;
		int posY = 0;

		try {
			posX = Integer.parseInt(preferences.get("jmyoggradioplayer_menucommon_posx", "0"));
			posY = Integer.parseInt(preferences.get("jmyoggradioplayer_menucommon_posy", "0"));
			Protocol.write("MenuCommon::posx:" + posX);
			Protocol.write("MenuCommon::posy:" + posY);

		} catch (Exception e) {
			Protocol.write("MenuCommon::Exception:");
			Protocol.write(e.toString());
			Protocol.write("Will position to (0,0)");
			posX = 0;
			posY = 0;
		}
		frame.setLocation(posX,posY);
		frame.pack();
		common = provider.getCommon();
		if (common.size() > Global.minCommon) localStore.putCommon(common);
		if (common.size() < Global.minCommon) common = localStore.getCommon();
		sppC.setName("Common");
		sppC.setPair(common);
		cpan.setLayout(new BorderLayout());
		cpan.add(sppC,BorderLayout.CENTER);
		cpan.add(buildPlayerMenuPanel(),BorderLayout.SOUTH);
		frame.setContentPane(cpan);
		versionMenuItem.addActionListener(this);
		wikiMenuItem.addActionListener(this);
		linuxPreferencesMenuItem.addActionListener(this);
		windowsPreferencesMenuItem.addActionListener(this);
		infoMenu.add(versionMenuItem);
		infoMenu.add(wikiMenuItem);
		linuxSettingsMenu.add(linuxPreferencesMenuItem);
		windowsSettingsMenu.add(windowsPreferencesMenuItem);
		menuBar.add(infoMenu);
		menuBar.add(linuxSettingsMenu);
		menuBar.add(windowsSettingsMenu);
		frame.setJMenuBar(menuBar);
	}

	private JPanel buildPlayerMenuPanel()
	{
		URL playIconURL = getClass().getResource("/images/buttons/play.png");
		Icon playIcon = new ImageIcon(playIconURL);
		URL stopIconURL = getClass().getResource("/images/buttons/stop.png");
		Icon stopIcon = new ImageIcon(stopIconURL);
		URL recordIconURL = getClass().getResource("/images/buttons/record.png");
		Icon recordIcon =  new ImageIcon(recordIconURL);
		URL loginIconURL = getClass().getResource("/images/buttons/logon.png");
		Icon loginURL = new ImageIcon(loginIconURL);
		URL logViewIconURL = getClass().getResource("/images/buttons/messages.png");
		Icon logViewIcon = new ImageIcon(logViewIconURL);
		playStreamButton.setIcon(playIcon);
		recordStreamButton.setIcon(recordIcon);
		stopStreamButton.setIcon(stopIcon);
		loginButton.setIcon(loginURL);
		showLogViewButton.setIcon(logViewIcon);
		JPanel erg = new JPanel();
		erg.setLayout(new GridLayout(2,1));
		JPanel bpan1 = new JPanel();
		JPanel bpan2 = new JPanel();
		bpan1.setLayout(new GridLayout(1,4));
		bpan1.add(playStreamButton);
		bpan1.add(recordStreamButton);
		bpan1.add(stopStreamButton);
		bpan1.add(loginButton);
		bpan2.setLayout(new GridLayout(1,1));
		bpan2.add(showLogViewButton);
		erg.add(bpan1);
		erg.add(bpan2);
		playStreamButton.addActionListener(this);
		recordStreamButton.addActionListener(this);
		stopStreamButton.addActionListener(this);
		loginButton.addActionListener(this);
		showLogViewButton.addActionListener(this);
		return erg;
	}

	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		StringPair spC = sppC.getSelected();
		if (source == playStreamButton) {
			if (spC != null) {
				stop();
				Player player = Factory.getPlayer();
				player.play(spC.getWert());
				TouchURL turl = Factory.getTouchURL();
				turl.touch(spC.getWert());
			}
			else
			{
				Protocol.write("MenuMain:actionPerformed:play:nothing selected");
			}
		}
		if (source == recordStreamButton) // record
		{
			if (spC != null)
			{
				stop();
				Recorder recorder = Factory.getRecorder();
				recorder.record(spC.getWert());
				TouchURL turl = Factory.getTouchURL();
				turl.touch(spC.getWert());
			}
			else
			{
				Protocol.write("MenuMain:actionPerformed:record:nothing selected");
			}
		}
		if (source == stopStreamButton) //stop
		{
			Protocol.write("MenuCommon:stopStreamButton:stop pressed");
			stop();
		}
		if (source == loginButton) // login
		{
			Menu ml = Factory.getMenuLogon();
			ml.show();
			frame.dispose();
		}
		if (source == showLogViewButton) // Messages
		{
			Menu pm = Factory.getProtocolMenu();
			pm.show();
		}
		if (source == versionMenuItem) // Version Info
		{
			JOptionPane.showMessageDialog(
					null,
					"JMyOggRadioPlayer Version " + Global.version,
					"",
					JOptionPane.INFORMATION_MESSAGE
			);
		}
		if (source == wikiMenuItem) // Wiki
		{
			String wiki = "http://wiki.ubuntuusers.de/JMyOggRadioPlayer";
			URL url = null;
			try
			{
				url = new URL(wiki);
				Desktop desktop = Desktop.getDesktop();
				desktop.browse(url.toURI());
			}
			catch (Exception e)
			{
				Protocol.write("MenuCommon:actionPerformed:wikiMenuItem:Exception:");
				Protocol.write(e.toString());
			}
		}
		if (source == linuxPreferencesMenuItem)
		{
			Menu lpm = Factory.getLinuxPreferencesMenu();
			lpm.show();
		}
		if (source == windowsPreferencesMenuItem)
		{
			Menu wpm = Factory.getWindowsPreferencesMenu();
			wpm.show();
		}
	}
	private void stop()
	{
		if (Global.process != null)
		{
			Global.process.destroy();
			Global.process = null;
			Protocol.write("MenuCommon:stop");
		}
		try
		{
			Thread.sleep(Global.stopDelta);
		}
		catch (Exception e)
		{
			//
		}
	}
	@Override
	public int onClose()
	{
		Point p = frame.getLocation();
		Protocol.write("MenuCommon:onClose:posx:" + p.x);
		Protocol.write("MenuCommon:onClose:posy:" + p.y);
		preferences.put("jmyoggradioplayer_menucommon_posx","" + p.x);
		preferences.put("jmyoggradioplayer_menucommon_posy","" + p.y);
		return Menu.DISPOSE;
	}
}
