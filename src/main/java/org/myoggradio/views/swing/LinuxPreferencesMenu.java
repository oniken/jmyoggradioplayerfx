package org.myoggradio.views.swing;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.util.prefs.Preferences;

import org.myoggradio.Global;
import org.myoggradio.interfaces.Protocol;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author homer65x
 * @modified_by Cronut
 */
public class LinuxPreferencesMenu extends Menu
{
	public static final long serialVersionUID= 0;
	private JPanel contentPanel = new JPanel();
	private JPanel modifyPreferencesPanel = new JPanel();
	private JLabel playerCommandLabel = new JLabel("Player Command");
	private JTextField playerCommandTextField = new JTextField(20);
	private JButton defaultPlayerCommandButton = new JButton("Default");
	private JLabel recorderCommandLabel = new JLabel("Recorder Command");
	private JTextField recoderCommandTextField = new JTextField(20);
	private JButton defaultRecorderCommandButton = new JButton("Default");
	private JLabel recorderDirectoryLabel = new JLabel("Recorder Directory");
	private JTextField recorderDirectoryTextField = new JTextField(20);
	private JButton defaultRecorderDirectoryButton = new JButton("Default");
	private JPanel buttonsGridPanel = new JPanel();
	private JPanel buttonsBorderPanel = new JPanel();
	private JButton saveButton = new JButton("save");
	private JButton cancelButton = new JButton("cancel");
	private Preferences preferences = Preferences.userRoot();


	public LinuxPreferencesMenu()
	{
		super("Linux Preferences");
		playerCommandTextField.setText(Global.linux_playercommand);
		recoderCommandTextField.setText(Global.linux_recordercommand);
		recorderDirectoryTextField.setText(Global.linux_recorderdirectory);

		modifyPreferencesPanel.setLayout(new GridLayout(3,3));
		modifyPreferencesPanel.add(playerCommandLabel);
		modifyPreferencesPanel.add(playerCommandTextField);
		modifyPreferencesPanel.add(defaultPlayerCommandButton);
		modifyPreferencesPanel.add(recorderCommandLabel);
		modifyPreferencesPanel.add(recoderCommandTextField);
		modifyPreferencesPanel.add(defaultRecorderCommandButton);
		modifyPreferencesPanel.add(recorderDirectoryLabel);
		modifyPreferencesPanel.add(recorderDirectoryTextField);
		modifyPreferencesPanel.add(defaultRecorderDirectoryButton);

		buttonsGridPanel.setLayout(new GridLayout(1,2));
		buttonsGridPanel.add(saveButton);
		buttonsGridPanel.add(cancelButton);
		buttonsBorderPanel.setLayout(new BorderLayout());
		buttonsBorderPanel.add(buttonsGridPanel,BorderLayout.WEST);

		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(modifyPreferencesPanel,BorderLayout.CENTER);
		contentPanel.add(buttonsBorderPanel,BorderLayout.SOUTH);

		frame.setContentPane(contentPanel);
		defaultPlayerCommandButton.addActionListener(this);
		defaultRecorderCommandButton.addActionListener(this);
		defaultRecorderDirectoryButton.addActionListener(this);
		saveButton.addActionListener(this);
		cancelButton.addActionListener(this);

		int posX = 0;
		int posY = 0;

		try
		{
			posX = Integer.parseInt(preferences.get("jmyoggradioplayer_linuxpreferencesmenu_posx", "0"));
			posY = Integer.parseInt(preferences.get("jmyoggradioplayer_linuxpreferencesmenu_posy", "0"));
			Protocol.write("LinuxPreferencesMenu::posx:" + posX);
			Protocol.write("LinuxPreferencesMenu::posy:" + posY);
		}
		catch (Exception e)
		{

		}
		frame.setLocation(posX,posY);
		frame.pack();
	}
	public void actionPerformed(ActionEvent event)
	{
		Object eventSource = event.getSource();
		if (eventSource == defaultPlayerCommandButton)
		{
			playerCommandTextField.setText(Global.linux_playercommand_default);
		}
		if (eventSource == defaultRecorderCommandButton)
		{
			recoderCommandTextField.setText(Global.linux_recordercommand_default);
		}
		if (eventSource == defaultRecorderDirectoryButton)
		{
			recorderDirectoryTextField.setText(Global.linux_recorderdirectory_default);
		}
		if (eventSource == saveButton)
		{
			String playerCommandText = playerCommandTextField.getText();
			String recorderCommandText = recoderCommandTextField.getText();
			String recorderDirectoryText = recorderDirectoryTextField.getText();
			Global.linux_playercommand = playerCommandText;
			Global.linux_recordercommand = recorderCommandText;
			Global.linux_recorderdirectory = recorderDirectoryText;
			preferences.put("jmyoggradioplayer_linux_playercommand",playerCommandText);
			preferences.put("jmyoggradioplayer_linux_recordercommand",recorderCommandText);
			preferences.put("jmyoggradioplayer_linux_recorderdirectory",recorderDirectoryText);
			Protocol.write("LinuxPreferencesMenu:jmyoggradio_linux_playercommand:" + playerCommandText);
			Protocol.write("LinuxPreferencesMenu:jmyoggradio_linux_recordercommand:" + recorderCommandText);
			Protocol.write("LinuxPreferencesMenu:jmyoggradio_linux_recorderdirectory:" + recorderDirectoryText);
		}
		if (eventSource == cancelButton)
		{
			onClose();
			frame.dispose();
		}
	}
	@Override
	public int onClose()
	{
		Point point = frame.getLocation();
		Protocol.write("LinuxPreferencesMenu:onClose:posx:" + point.x);
		Protocol.write("LinuxPreferencesMenu:onClose:posy:" + point.y);
		preferences.put("jmyoggradioplayer_linuxpreferencesmenu_posx","" + point.x);
		preferences.put("jmyoggradioplayer_linuxpreferencesmenu_posy","" + point.y);
		return Menu.DISPOSE;
	}
}
