package org.myoggradio;

import org.myoggradio.interfaces.Command;
import org.myoggradio.interfaces.Protocol;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class AdvancedCommand implements Command
{
	public int start(String command)
	{
		int rc = 0;
		if (Global.process != null)
		{
			Protocol.write("AdvancedCommand:Command already running");
			rc = 4;
			return rc;
		}
		Runtime rt = Runtime.getRuntime();
		try
		{
			Protocol.write("AdvancedCommand:start:" + command);
			Process p = rt.exec(command);
			Global.process = p;
			InputStream is = p.getInputStream();
			Reader r = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(r);
			String satz = br.readLine();
			long n = 0;
			while (satz != null)
			{
				n++;
				if (n > 15)
				{
					// Mache nichts
				}
				else if (n == 15)
				{
					Protocol.write("AdvancedCommand:write no more messages");
				}
				else
				{
					Protocol.write(satz);
				}
				satz = br.readLine();
			}
			rc = p.waitFor();
			Protocol.write("AdvancedCommand:end with Return Code:" + rc);
		}
		catch (Exception e)
		{
			Protocol.write("SimpleCommand:start:Exception:");
			Protocol.write(e.toString());
		}
		if (Global.process != null)
		{
			Global.process.destroy();
		}
		Global.process = null;
		return rc;
	}
}
