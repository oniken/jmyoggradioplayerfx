package org.myoggradio;
import java.io.*;
import java.net.*;
import java.util.*;
import org.myoggradio.interfaces.*;
import org.myoggradio.interfaces.Provider;
import org.myoggradio.stringpair.StringPair;

public class MyOggRadioProvider implements Provider
{
	public void changeFavorite(StringPair sp, String user, String password)
	{
		String name = sp.getName();
		String xurl = sp.getWert();
		try
		{
			user = URLEncoder.encode(user,"UTF-8");
			password = URLEncoder.encode(password,"UTF-8");
			xurl = URLEncoder.encode(xurl,"UTF-8");
			name = URLEncoder.encode(name,"UTF-8");
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:changeFavorite:Exception:");
			Protocol.write(e.toString());
		}
		String surl = Global.myoggradio + "/p_change.jsp";
		surl += "?user=" + user;
		surl += "&password=" + password;
		surl += "&url=" + xurl;
		surl += "&name=" + name;
		Protocol.write("MyOggRadioProvider:changeFavorite:will open:");
		Protocol.write(surl);
		try
		{
			URL url = new URL(surl);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String erg = "";
			String satz = br.readLine();
			while (satz != null)
			{
				erg += satz;
				satz = br.readLine();
			}
			br.close();
			erg = extract(erg);
			Protocol.write("MyOggRadioProvider:changeFavorite:" + erg);
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:changeFavorite:Exception:");
			Protocol.write(e.toString());
		}
	}
	public void addFavorite(StringPair sp, String user, String password)
	{
		String xurl = sp.getWert();
		try
		{
			user = URLEncoder.encode(user,"UTF-8");
			password = URLEncoder.encode(password,"UTF-8");
			xurl = URLEncoder.encode(xurl,"UTF-8");
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:addFavorite:Exception:");
			Protocol.write(e.toString());
		}
		String surl = Global.myoggradio + "/p_add2.jsp";
		surl += "?user=" + user;
		surl += "&password=" + password;
		surl += "&url=" + xurl;
		Protocol.write("MyOggRadioProvider:addFavorite:will open:");
		Protocol.write(surl);
		try
		{
			URL url = new URL(surl);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String erg = "";
			String satz = br.readLine();
			while (satz != null)
			{
				erg += satz;
				satz = br.readLine();
			}
			br.close();
			erg = extract(erg);
			Protocol.write("MyOggRadioProvider:addFavorite:" + erg);
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:addFavorite:Exception:");
			Protocol.write(e.toString());
		}
	}
	public void addNewURL(String surl,String bemerkung,String benutzer, String passwort) 
	{
		try
		{
			benutzer = URLEncoder.encode(benutzer,"UTF-8");
			passwort = URLEncoder.encode(passwort,"UTF-8");
			surl = URLEncoder.encode(surl,"UTF-8");
			bemerkung = URLEncoder.encode(bemerkung,"UTF-8");
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:addNewURL:Exception:");
			Protocol.write(e.toString());
		}
		String xurl = Global.myoggradio + "/p_newURL.jsp";
		xurl += "?user=" + benutzer;
		xurl += "&password=" + passwort;
		xurl += "&url=" + surl;
		xurl += "&bemerkung=" + bemerkung;
		Protocol.write("MyOggRadioProvider:addNewURL:will open:");
		Protocol.write(xurl);
		try
		{
			URL url = new URL(xurl);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String erg = "";
			String satz = br.readLine();
			while (satz != null)
			{
				erg += satz;
				satz = br.readLine();
			}
			br.close();
			erg = extract(erg);
			Protocol.write("MyOggRadioProvider:addNewURL:" + erg);
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:addNewURL:Exception:");
			Protocol.write(e.toString());
		}
	}
	public void dropFavorite(StringPair sp, String benutzer, String passwort)
	{
		String xurl = sp.getWert();
		try
		{
			benutzer = URLEncoder.encode(benutzer,"UTF-8");
			passwort = URLEncoder.encode(passwort,"UTF-8");
			xurl = URLEncoder.encode(xurl,"UTF-8");
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:changeFavorite:Exception:");
			Protocol.write(e.toString());
		}
		String surl = Global.myoggradio + "/p_drop2.jsp";
		surl += "?user=" + benutzer;
		surl += "&password=" + passwort;
		surl += "&url=" + xurl;
		try
		{
			URL url = new URL(surl);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String erg = "";
			String satz = br.readLine();
			while (satz != null)
			{
				erg += satz;
				satz = br.readLine();
			}
			br.close();
			erg = extract(erg);
			Protocol.write("MyOggRadioProvider:dropFavorite:" + erg);
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:dropFavorite:Exception:");
			Protocol.write(e.toString());
		}		
	}
	public ArrayList<StringPair> getCommon()
	{
		ArrayList<StringPair> erg = new ArrayList<StringPair>();
		try
		{
			URL url = new URL(Global.myoggradio + "/common.json");
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String json = "";
			String satz = br.readLine();
			while (satz != null)
			{
				json += satz;
				satz = br.readLine();
			}
			br.close();
			String jsonx = "";
			for (int i=0;i<json.length();i++)
			{
				String ch = json.substring(i,i+1);
				if (ch.equals("{"))
				{
					jsonx = "";
				}
				else if (ch.equals("}"))
				{
					StringPair sp = getStringPaar(jsonx);
					erg.add(sp);
				}
				else
				{
					jsonx += ch;
				}
			}
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:getCommon:Exception:");
			Protocol.write(e.toString());
		}
		return erg;
	}
	public ArrayList<StringPair> getFavoriten(String benutzer, String passwort)
	{
		ArrayList<StringPair> erg = new ArrayList<StringPair>();
		try
		{
			URL url = new URL(Global.myoggradio + "/player.json?user=" + benutzer + "&password=" + passwort);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String json = "";
			String satz = br.readLine();
			while (satz != null)
			{
				json += satz;
				satz = br.readLine();
			}
			br.close();
			String jsonx = "";
			for (int i=0;i<json.length();i++)
			{
				String ch = json.substring(i,i+1);
				if (ch.equals("{"))
				{
					jsonx = "";
				}
				else if (ch.equals("}"))
				{
					StringPair sp = getStringPaar(jsonx);
					erg.add(sp);
				}
				else
				{
					jsonx += ch;
				}
			}
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:getFavoriten:Exception:");
			Protocol.write(e.toString());
		}
		return erg;
	}
	private StringPair getStringPaar(String jsonx)
	{
		StringPair erg = new StringPair();
		String name = "";
		String wert = "";
		String temp = "";
		int anz=0;
		for (int i=0;i<jsonx.length();i++)
		{
			String ch = jsonx.substring(i,i+1);
			if (ch.equals("\""))
			{
				anz++;
				if (anz == 4) wert = temp;
				if (anz == 8) name = temp;
				temp = "";
			}
			else
			{
				temp += ch;
			}
		}
		erg.setName(name);
		erg.setWert(wert);
		return erg;
	}
	public String login(String benutzer, String passwort) 
	{
		String erg = "unbekannter Fehler";
		try
		{
			URL url = new URL(Global.myoggradio + "/p_login.jsp?user=" + benutzer + "&password=" + passwort);
			URLConnection con = url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			erg = "";
			String satz = br.readLine();
			while (satz != null)
			{
				erg += satz;
				satz = br.readLine();
			}
			br.close();
			erg = extract(erg);
		}
		catch (Exception e)
		{
			Protocol.write("MyOggRadioProvider:login:Exception:");
			Protocol.write(e.toString());
			erg = "Exception";
		}
		return erg;
	}
	private String extract(String s)
	{
		String erg = "";
		int l = s.length();
		boolean ok = false;
		for (int i=0;i<l;i++)
		{
			String ch = s.substring(i,i+1);
			if (ch.equals("["))
			{
				ok = true;
			}
			else if (ch.equals("]"))
			{
				ok = false;
			}
			else
			{
				if (ok) erg += ch;
			}
		}
		return erg;
	}
}
